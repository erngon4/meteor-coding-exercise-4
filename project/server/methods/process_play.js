/*****************************************************************************/
/* ProcessPlay Methods */
/*****************************************************************************/

Meteor.methods({
 '/app/processPlay': function (scenario,npCharacter,npCharacterRequest,playerResponse,playerResponseCode) {

  var user = Meteor.user();

  var score = user.profile.score;

  var counter = user.profile.counter;

  if (score === undefined) {

   score = 0;

  }

  if (counter === undefined) {

   counter = 0;

  }


  var resultMessage;

  var points = 0;

  // Evaluate input elements
  // In this case, results are completely random
  var randomResult = Math.random() < 0.5 ? true : false; // http://stackoverflow.com/questions/9730966/how-to-decide-between-two-numbers-randomly-using-javascript

  for(var i = 0; i < playerResponse.length; i++) {
      if(playerResponse[i].objectCode == playerResponseCode)
      {
       points = i;
       counter = counter +1;
      }
  }


  resultMessage = "Point get!";
  // If necessary increment user's score
  score = score + points;

  // If necessary select a

  // new scenario,
  // var scenarios = Scenario.find ({}).fetch ();

  // character,
  // var npCharacters = NPCharacter.find ({scenarioCodes: randomScenario.objectCode}).fetch ();

  // and request,
  // var requests = NPCharacterRequest.find ({objectCode: {$in: randomCharacter.npCharacterRequestCodes}}).fetch ();

  // and return them to the template


  Meteor.users.update({_id:Meteor.user()._id}, {$set:{"profile.score":score}});
  Meteor.users.update({_id:Meteor.user()._id}, {$set:{"profile.counter":counter}});

   return {randomScenario:true,playResult:resultMessage,newScenario:null,newNPCharacter:null,newNPCharacterRequest:null};

 }

});
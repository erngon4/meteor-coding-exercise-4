// Something that the non playable character will ask of the player

NPCharacterRequest = new Mongo.Collection('n_p_character_request');

NPCharacterRequest.attachSchema(new SimpleSchema({

  title:{
    type:String
  },
  objectCode:{
    type:String
  },
  playerResponseCodes:{
    type:[String],
    optional: true
  }

}));